package com.example.demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AsyntestApplication {

	public static void main(String[] args) {
		SpringApplication.run(AsyntestApplication.class, args);
	}
}
